/**
 *The server that communicates with the client and has a threadpool to run the 
 * toolshop class
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.model.ToolShop;

/**
 * The class that communicates with the client and with the model
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class TheServer {

    /**
     * the server socket
     */
    private ServerSocket theServerSocket;
    /**
     * the socket that accepts the client socket
     */
    private Socket theSocket;
    /**
     * The ToolShop object
     */
    private ToolShop theShop;
    /**
     * the bufferedreader that writes to theSocket
     */
    private BufferedReader input;
    /**
     * the printWriter that rights to the socket
     */
    private PrintWriter output;
    /**
     * thread pool to handle communication
     */
    private ExecutorService theThreadPool;

    /**
     * constructs an object of type server
     */
    protected TheServer() {

        try {
            theServerSocket = new ServerSocket(9456);
            theThreadPool = Executors.newFixedThreadPool(5);
            System.out.println("Server is running");
        } catch (IOException e) {
            System.out.println("Error starting server");
            System.out.println(e.getMessage());
        }

    }

    /**
     * communicates with the Client application
     *
     * @throws IOException
     */
    protected void runServer() {

        try {
            while (true) {
                theSocket = theServerSocket.accept();
                System.out.println("After accept");
                ToolShop theShop = new ToolShop(theSocket);

                theThreadPool.execute(theShop);

            }
        } catch (IOException f) {
            System.out.println(f.getMessage());

            theThreadPool.shutdown();
            try {
                theSocket.close();
            } catch (IOException g) {
                System.out.println(g.getMessage());
            }

        }

    }

    public static void main(String[] args) {
        TheServer myServer = new TheServer();
        myServer.runServer();

    }

}

/**
 * contains the information about orders the shop makes
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Order {
	
	/**
	 * the date each order is made on
	 */
	private Calendar date;
	/**
	 * a list of all the orderlines in an order
	 */
	private ConcurrentLinkedQueue<OrderLine> order;
	/**
	 * the order id
	 */
	private int id;

	/**
	 * constructs an object of type Order
	 */
	public Order() {
		date = Calendar.getInstance();
		order = new ConcurrentLinkedQueue<OrderLine>();
		Random random = new Random();
		id = 10000 + random.nextInt(100000);
	}

	/**
	 * Adds orderline
	 * 
	 * @param an orderline to be added
	 */
	public void add(OrderLine placeOrder) {
		order.add(placeOrder);
	}

	/**
	 * Prints the current order to the text file
	 * 
	 * @return a String that contains the status of the file
	 */
	public synchronized String printOrderToFile(String path) {
		  try {
	            Thread.sleep(1000);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
		try (PrintWriter p = new PrintWriter(new BufferedWriter((new FileWriter((new File(path)), true))))) {
			p.append("ORDER ID:			" + id + "\r\nDate Ordered:			" + date.getTime() + "\r\n");
			Object []theNewOrder=order.toArray();
			for (int i = 0; i < theNewOrder.length; i++) {
				p.append("\r\n");
				p.append(theNewOrder[i].toString());
			}
			p.append("\r\n");
		} catch (IOException e) {
			e.printStackTrace();
			return "FAILED";

		}
		return "SUCCESS";
	}

}

/**
 * Stores information about an item that will be ordered
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

public class OrderLine {

	/**
	 * the item that is being ordered
	 */
	private Tool theTool;
	/**
	 * the quantity of item being ordered
	 */
	private int quantity;

	/**
	 * constructs an object of type OrderLine
	 */
	public OrderLine(Tool toBeOrdered, int quantity) {
		theTool = toBeOrdered;
		this.quantity = quantity;
	}

	/**
	 * Constructs a String of all the tool information and quantity needed for the
	 * orderline
	 * 
	 * @return String orderLine information.
	 */
	public String toString() {
		return "Item Name: " + theTool.getToolName() + ", Item ID: " + theTool.getToolId() + "\n" + "Order Quantity: "
				+ quantity + "\n";
	}

}

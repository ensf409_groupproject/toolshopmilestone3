/**
 * This class allows you to create and manage database
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 */
package server.model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DatabaseController implements IDBCredentials {

	// Attributes
	private Connection conn;
	public String databaseName = "toolshop";
	public String itemsTableName = "items";
	public String suppliersTableName = "suppliers";

	/**
	 * constructs an object of type DatabaseController
	 */

	public DatabaseController() {
		initializeConnection();
	}

	/**
	 * This method Gets a connection to database
	 */
	public void initializeConnection() {
		try {
			// Register JDBC driver
			Driver driver = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(driver);

			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			checkIfDatabaseExistsAndIfItDoesntMakeTheDatabase();

		} catch (SQLException e) {
			System.out.println("Problem");
			e.printStackTrace();
		}

	}


	/**
	 * Check If Database Exists And If It Doesn't Make The Database and tables items
	 * and supplier
	 * 
	 * @throws SQLException
	 */

	private void checkIfDatabaseExistsAndIfItDoesntMakeTheDatabase() throws SQLException {
		if (conn != null) {
			DatabaseMetaData dmd = conn.getMetaData();
			ResultSet theCatalogs = dmd.getCatalogs();

			boolean databaseExists = false;

			while (theCatalogs.next()) {
				String catalogs = theCatalogs.getString(1);
				if (catalogs.equals(databaseName)) {
					System.out.println(databaseName + " Exists");
					databaseExists = true;
					break;
				}
			}

			if (!databaseExists) {
				createDB();
				// create items
				createItemsTable();
				fillItemsTable();
				// create suppliers
				createSuppliersTable();
				fillSuppliersTable();
			}

		}
	}


	/**
	 * This method creates database
	 */

	public void createDB() {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("CREATE DATABASE " + databaseName);
			System.out.println("Database " + databaseName + " is created.");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Problem creating database");
			e.printStackTrace();
		}
	}


	/**
	 * This method creates Items Table
	 */

	public void createItemsTable() {
		String sql = "CREATE TABLE " + itemsTableName
				+ "( ID INT(4) NOT NULL, TOOLNAME VARCHAR(20) NOT NULL, QUANTITY INT(4) NOT NULL, PRICE DOUBLE(5,2) NOT NULL, SUPPLIERID INT(4) NOT NULL, PRIMARY KEY ( id ))";
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			System.out.println("Created Table " + itemsTableName);
		} catch (SQLException e) {
			System.out.println("Problem creating table");
			e.printStackTrace();
		}
	}


	/**
	 * This methods reads data from items.txt to fill the Items table
	 */

	public void fillItemsTable() {
		try (Scanner scan = new Scanner(new FileReader("items.txt"))) {

			while (scan.hasNext()) {
				String toolInfo[] = scan.nextLine().split(";");
				insertIntoItemTable(new Tool(toolInfo[1], Integer.parseInt(toolInfo[0]), Integer.parseInt(toolInfo[2]),
						Double.parseDouble(toolInfo[3]), Integer.parseInt(toolInfo[4])));

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * This method creates the supplier table
	 */

	public void createSuppliersTable() {
		String sql = "CREATE TABLE " + suppliersTableName
				+ "( ID INT(4) NOT NULL, SUPPLIERNAME VARCHAR(40) NOT NULL, SUPPLIERADDRESS VARCHAR(40) NOT NULL, SALESCONTACT VARCHAR(20) NOT NULL, PRIMARY KEY ( id ))";
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			System.out.println("Created Table " + suppliersTableName);
		} catch (SQLException e) {
			System.out.println("Problem creating table");
			e.printStackTrace();
		}
	}


	/**
	 * This methods reads data from suppliers.txt to fill the Supplier table
	 */

	public void fillSuppliersTable() {
		try (Scanner scan = new Scanner(new FileReader("suppliers.txt"))) {

			while (scan.hasNext()) {
				String supplierInfo[] = scan.nextLine().split(";");
				insertIntoSupplierTable(new Supplier(Integer.parseInt(supplierInfo[0]), supplierInfo[1],
						supplierInfo[2], supplierInfo[3]));

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method used to add data to the database
	 * 
	 * @param tool the tool to be addedto the database
	 * @return true if insertion is successful, returns false otherwise
	 */

	public boolean insertIntoItemTable(Tool tool) {
		try {
			String query = "INSERT INTO " + itemsTableName
					+ " (ID, TOOLNAME, QUANTITY, PRICE, SUPPLIERID) values(?,?,?,?,?)";
			PreparedStatement pStat = conn.prepareStatement(query);
			pStat.setInt(1, tool.getToolId());
			pStat.setString(2, tool.getToolName());
			pStat.setInt(3, tool.getQuantity());
			pStat.setDouble(4, tool.getPrice());
			pStat.setInt(5, tool.getSupplierID());
			int rowCount = pStat.executeUpdate();
			System.out.println("row Count = " + rowCount);
			pStat.close();
			return true;
		} catch (SQLException e) {
			System.out.println("problem inserting tool");
			e.printStackTrace();
		}
		return false;

	}


	/**
	 * Used to add supplier to the database
	 * 
	 * @param theSupplier
	 * @return returns true if successful, returns false if it fails
	 */

	public boolean insertIntoSupplierTable(Supplier theSupplier) {
		try {
			String query = "INSERT INTO " + suppliersTableName
					+ " (ID, SUPPLIERNAME, SUPPLIERADDRESS, SALESCONTACT) values(?,?,?,?)";
			PreparedStatement pStat = conn.prepareStatement(query);
			pStat.setInt(1, theSupplier.getSupplierId());
			pStat.setString(2, theSupplier.getSupplierName());
			pStat.setString(3, theSupplier.getAddress());
			pStat.setString(4, theSupplier.getSalesContact());
			int rowCount = pStat.executeUpdate();
			System.out.println("row Count = " + rowCount);
			pStat.close();
			return true;
		} catch (SQLException e) {
			System.out.println("problem inserting tool");
			e.printStackTrace();
		}
		return false;

	}


	/**
	 * This method closes the connection
	 */

	public void close() {
		try {
			// rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/**
	 * shows all columns of items table
	 * 
	 * @return tools
	 */
	public ResultSet ShowItems() {
		try {
			String sql = "SELECT * FROM " + itemsTableName;
			Statement stmt = conn.createStatement();
			ResultSet tools = stmt.executeQuery(sql);


			return tools;
		} catch (Exception e) {
			System.out.println("Problem Showing table");
			return null;
		}

	}




	/**
	 * 
	 * This method should search the items table for a tool matching the toolID It
	 * should return null if no tools matching that ID are found.
	 * 
	 * @param toolID int that is toolID
	 * @return tool the tool
	 */
	public Tool searchToolbyID(int toolID) {
		ResultSet tool;
		try {
			String query = "SELECT * FROM " + itemsTableName + " WHERE ID = ?";
			PreparedStatement pStat = conn.prepareStatement(query);
			pStat.setInt(1, toolID);
			tool = pStat.executeQuery();
			if (tool.next()) {
				return new Tool(tool.getString("TOOLNAME"), tool.getInt("ID"), tool.getInt("QUANTITY"),
						tool.getDouble("PRICE"), tool.getInt("SUPPLIERID"));
			}
			pStat.close();
		} catch (SQLException e) {
			System.out.println("problem searching tool");
			e.printStackTrace();
		}



		return null;
	}

	/**
	 * This method should search the items table for a tool matching the toolName It
	 * should return null if no tools matching that ID are found.
	 * 
	 * @param toolName
	 * @return tool
	 */
	public Tool searchToolbyName(String toolName) {
		ResultSet tool;
		try {
			String query = "SELECT * FROM " + itemsTableName + " WHERE TOOLNAME = ?";
			PreparedStatement pStat = conn.prepareStatement(query);
			pStat.setString(1, toolName);
			tool = pStat.executeQuery();
			if (tool.next()) {
				return new Tool(tool.getString("TOOLNAME"), tool.getInt("ID"), tool.getInt("QUANTITY"),
						tool.getDouble("PRICE"), tool.getInt("SUPPLIERID"));
			}
			pStat.close();
		} catch (SQLException e) {
			System.out.println("problem searching tool");
			e.printStackTrace();
		}

		return null;
	}



	/**
	 * This method update quantity by using toolName
	 * 
	 * @param toolName
	 * @param newQuantity
	 * @return true/false
	 */
	public boolean updateQuantity(String toolName, int newQuantity) {

		String query = "update items set QUANTITY= ? where TOOLNAME= ?";
		try (PreparedStatement pStat = conn.prepareStatement(query)) {
			pStat.setInt(1, newQuantity);
			pStat.setString(2, toolName);
			pStat.executeUpdate();
		} catch (SQLException m) {
			System.out.println("problem selling tool");
			m.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * shows full tool list
	 * 
	 * @return tools.toString()
	 */
	public String fullToolList() {
		ResultSet toolList;
		try {
			String query = "SELECT * FROM " + itemsTableName;
			PreparedStatement pStat = conn.prepareStatement(query);
			toolList = pStat.executeQuery();
			StringBuilder tools = new StringBuilder();
			while (toolList.next()) {
			tools.append(convertResultToString(toolList));
				tools.append(" @ ");


			}
			pStat.close();
			return tools.toString();
		} catch (SQLException e) {
			System.out.println("problem searching tool");
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * This method convert result to string
	 * 
	 * @param toolList
	 * @return string
	 * @throws SQLException
	 */

	private String convertResultToString(ResultSet toolList) throws SQLException {
		int id = toolList.getInt("ID");
		String name = toolList.getString("TOOLNAME");
		int quantity = toolList.getInt("QUANTITY");
		double price = toolList.getDouble("PRICE");
		int supId = toolList.getInt("SUPPLIERID");
		return id + ";" + name + ";" + quantity + ";" + price + ";" + supId;
	}

}

/**
 * Contains all the information needed to run the tool shop
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ToolShop implements Runnable {

    /**
     * the toolshop inventory
     */
    Inventory theInventory;
    /**
     * the socket to communicate with client applications
     */
    private Socket theSocket;
    /**
     * print writer to send data to the client
     */
    private PrintWriter output;
    /**
     * bufferedReader to read data from the client
     */
    private BufferedReader input;

    /**
     * constructs an object of type ToolShop
     */
    public ToolShop(Socket theSocket) {
        theInventory = new Inventory();
        this.theSocket = theSocket;

        try {
            input = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
            output = new PrintWriter(theSocket.getOutputStream(), true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     *	This method returns theInventory
     * @return theInventory
     */
    public Inventory getInventory() {
        return theInventory;
    }

    /**
     * calls the inventory function sell Tool and processes and returns a
     * different string depending on the success of the sellTool function
     *
     * @param name the name of the tool being purchased
     * @param quantity the quantity being purchased
     * @return a String containing "SUCCESS" if the tool quantity was decreased,
     * otherwise returns the instruction from the sellTool function
     */
    public String attemptPurchase(String name, int quantity) {
        String instruction = theInventory.sellTool(name, quantity);
        if (instruction.equals("ALL GOOD") || instruction.equals("ORDER MORE")) {
            return "SUCCESS";
        } else {
            return instruction;
        }
    }
    
    /**
     * this method invokes a  method for each button function in the client
     */
    @Override
    public void run() {
        while (true) {
            try {

                String response = input.readLine();
                switch (response) {
                    case "LIST":
                        list();
                        break;
                    case "SEARCH-NAME":
                        searchName();
                        break;
                    case "SEARCH-ID":
                        searchID();
                        break;
                    case "SELL":
                        sell();
                        break;
                    case "ADD":
                        addTool();
                        break;
                    case "GENERATE-ORDER":
                        generateOrder();
                        break;
                }

            } catch (IOException z) {
                System.out.println(z.getMessage());
                break;
            } catch (NullPointerException z) {
                break;
            }
        }
    }

    /**
     * calls the Inventory function that creates the order;
     *
     * @param path
     * @return test
     */
    public String printOrder(String path) {
        String test = theInventory.createOrder(path);
        return test;

    }
    /**
     * This method reads the path to where the file will be saved and then saves the file there
     */
    private void generateOrder() {
        try {
            String path = input.readLine();
            output.println(printOrder(path));
        } catch (IOException ex) {
            System.out.println("Invalid path");
        }
    }
    /**
     * calls the Inventory function that insert tools;
     * @throws IOException
     */
    private void addTool() throws IOException {
        String toolInfo = input.readLine();
        Tool toBeAdded = createToolfromString(toolInfo);
        boolean success = theInventory.insertTool(toBeAdded);
        if (success) {
            output.println("SUCCESS");
        } else {
            output.println("FAILED");
        }
        output.flush();
    }
    /**
     * This method create tool object from string 
     * @param toolInfo String that contains the info for the tool
     * @return tool tool object created from the information
     */
    private Tool createToolfromString(String toolInfo) {
        String[] s = toolInfo.split(";");

        int id = Integer.parseInt(s[0]);
        String name = s[1];
        int quantity = Integer.parseInt(s[2]);
        double price = Double.parseDouble(s[3]);
        int supId = Integer.parseInt(s[4]);
        Tool tool = new Tool(name, id, quantity, price, supId);
        return tool;

    }
    /**
     * This Method reads in the quantity of the item being sold and the item that is being sold and attempts to sell it
     * @throws IOException
     */
    private void sell() throws IOException {
        String[] quantityAndItem = input.readLine().split(" @ ");
        String instruction = attemptPurchase(quantityAndItem[1], Integer.parseInt(quantityAndItem[0]));
        if (instruction.equals("SUCCESS")) {
            output.println("SUCCESS");
        } else {
            output.println("FAILED");

            output.println(instruction);
        }
        output.flush();

    }
    /**
     * calls the Inventory function that search ID
     * @throws IOException
     */
    private void searchID() throws IOException {
        String idString = input.readLine();
        try {
            int id = Integer.parseInt(idString);
            output.println(theInventory.searchID(id));
            output.flush();
        } catch (NumberFormatException e) {
            output.println("Tool not found");
            output.flush();
        }
    }
    /**
     * calls the Inventory function that search Name
     * @throws IOException
     */
    private void searchName() throws IOException {
        String name = input.readLine();
        output.println(theInventory.searchName(name));
        output.flush();
    }
    /**
     * calls the Inventory function that display tools
     */
    private void list() {
        output.println(theInventory.displayTools());
        output.flush();
    }
}

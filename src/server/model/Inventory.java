/**
 * stores an object of ReadFromFile that contains the tools and functions to access them
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

public class Inventory {
	/**
	 * the dbController object
	 */
	DatabaseController dbController;

	/**
	 * An Order object that
	 */
	private Order theOrder;

	/**
	 * constructs an object of type Inventory
	 */
	public Inventory() {

		theOrder = new Order();
		dbController = new DatabaseController();
	}

	/**
	 * Calls a function in the database controller to find the tool if quantity
	 * allows it calls another function in dbController to decrease the quantity and
	 * returns a String with information about whether a Tool's quantity could be
	 * decreased
	 *
	 * @param name     the name of the tool
	 * @param quantity the quantity to be purchased
	 * @return instruction String containing information about whether the tool was
	 *         purchased
	 */
	public synchronized String sellTool(String name, int quantity) {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tool theTool = dbController.searchToolbyName(name);
		String instruction = "TOOL NOT FOUND!";
		if (theTool != null) {
			instruction = theTool.decreaseItemQuantity(quantity);
			if (instruction.equals("ORDER MORE")) {
				theOrder.add(theTool.placeOrder());
			}
			if (instruction.equals("ALL GOOD") || instruction.equals("ORDER MORE")) {
				if (!dbController.updateQuantity(name, theTool.getQuantity())) {
					instruction = "Error accessing inventory";
				}
			}
		}

		return instruction;
	}

	/**
	 * Calls the function in order to write the order to a text file.
	 *
	 * @return a String containing the status of the printing t file function
	 */
	public String createOrder(String path) {
		String theStringWeAreReturning = theOrder.printOrderToFile(path);
		theOrder = new Order();
		return theStringWeAreReturning;
	}

	/**
	 * calls the function in dbController to find the Tool based on id
	 * 
	 * @param id
	 * @return "Tool not found" or the tool information
	 */
	public String searchID(int id) {
		Tool searchedTool = dbController.searchToolbyID(id);
		if (searchedTool == null) {
			return "Tool not found";
		}
		return searchedTool.toString();
	}

	/**
	 * calls the function in dbController to find the Tool based on name
	 * 
	 * @param name
	 * @return "Tool not found" or the tool information
	 */
	public String searchName(String toolName) {
		Tool searchedTool = dbController.searchToolbyName(toolName);
		if (searchedTool == null) {
			return "Tool not found";
		}
		return searchedTool.toString();
	}

	/**
	 * calls the function in dbController to added the tool to the database
	 * 
	 * @param toBeAdded the tool to be added to the database
	 * @return true if the tool is added, else returns false
	 */
	public boolean insertTool(Tool toBeAdded) {
		return dbController.insertIntoItemTable(toBeAdded);
	}

	/**
	 * calls a function in dbController to get all the tool information
	 * 
	 * @return a long string containing all the tool data
	 */
	public String displayTools() {
		return dbController.fullToolList();
	}

}

/**
 * Contains data fields and methods to create and display the insert tool window of the
 * Tool Shop application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package client.views;

import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InsertToolDisplay extends JFrame {
	/**
	 * constructs an object of type InsertToolDisplay
	 */
	public InsertToolDisplay() {
		super();
		initComponents();
		this.setVisible(false);
		this.setSize(500, 200);
		this.setResizable(false);
	}

	/**
	 * Text Fields for the insert tool display that correspond to the purposes input id,
	 * input name, input quantity, and input price, and input supplier
	 * file
	 */    
	private JTextField inputID;
	private JTextField inputName;
	private JTextField inputQuantity;
	private JTextField inputPrice;
	private JTextField inputSupplierId;
	
	/**
	 * Button for adding item
	 */
    private JButton addButton;
	/**
	 *  Initializes buttons and panels for the insert tool view display
	 */
	private void initComponents() {
            
		inputID = new JTextField("", 10);
		inputName = new JTextField("", 10);
		inputQuantity = new JTextField("", 10);
		inputPrice = new JTextField("", 10);
		inputSupplierId = new JTextField("", 10);

        addButton = new JButton("Add");

		Container c = this.getContentPane();
		JPanel thePanel = new JPanel();

		thePanel.add(new JLabel("ID"));
		thePanel.add(inputID);
                
		thePanel.add(new JLabel("Name"));
		thePanel.add(inputName);
                
		thePanel.add(new JLabel("Quantity"));
		thePanel.add(inputQuantity);
                
		thePanel.add(new JLabel("Price"));
		thePanel.add(inputPrice);
                
		thePanel.add(new JLabel("Supplier ID"));
		thePanel.add(inputSupplierId);

		c.add(thePanel, "Center");

		thePanel = new JPanel();
		thePanel.add(addButton);
		c.add(thePanel, "South");

	}
	/**
	 * Returns the input of Id field
	 * @return inputID
	 */
	public JTextField getInputID() {
		return inputID;
	}

	/**
	 * Returns the input of Name field
	 * @return inputName
	 */
	public JTextField getInputName() {
		return inputName;
	}

	/**
	 * Returns the input of Quantity field
	 * @return inputQuantity
	 */
	public JTextField getInputQuantity() {
		return inputQuantity;
	}
	/**
	 * Returns the input of Price field
	 * @return inputPrice
	 */
	public JTextField getInputPrice() {
		return inputPrice;
	}
	/**
	 * Returns the input of Supplier field
	 * @return inputSupplierId
	 */
	public JTextField getInputSupplierId() {
		return inputSupplierId;
	}
	/**
	 * Returns the Add button for the frame
	 * @return addButton
	 */
	public JButton getAddButton() {
		return addButton;
	}
	
	/**
	 * This method clear entries of text fields
	 */
	public void clearEntries() {
		inputID.setText("");
		inputName.setText("");
		inputQuantity.setText("");
		inputPrice.setText("");
		inputSupplierId.setText("");

	}

}

package client.controller;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import client.views.*;

/**
 *
 * This class creates an objects for all the displays and controllers This class
 * contains the main
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class TheClient {

    private Socket theSocket;
    /**
     * constructs an object of type TheClient
     * @param serverName
     * @param portNumber
     */
    public TheClient(String serverName, int portNumber) {
        try {
            theSocket = new Socket(serverName, portNumber);
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * Initialized the displays and controllers, passing the socket to the controllers for communication with the server
     */
    public void communicate() {
        SearchToolDisplay theSearchToolDisplay = new SearchToolDisplay();
        SearchToolController theSearchToolController = new SearchToolController(theSearchToolDisplay, theSocket);

        DecreaseToolDisplay theDecreaseToolDisplay = new DecreaseToolDisplay();
        DecreaseToolController theDecreaseToolController = new DecreaseToolController(theDecreaseToolDisplay,
                theSocket);

        InsertToolDisplay theInsertToolDisplay = new InsertToolDisplay();
        InsertToolController theInsertToolController = new InsertToolController(theInsertToolDisplay, theSocket);

        MainViewDisplay theMainViewDisplay = new MainViewDisplay();
        MainViewController theMainViewController = new MainViewController(theMainViewDisplay, theSearchToolDisplay,
                theInsertToolDisplay, theDecreaseToolDisplay, theSocket);

    }

    public static void main(String[] args) {
        TheClient myClient = new TheClient("localhost", 9456);
        myClient.communicate();

    }

}

/**
 * This class is made up of nested classes that contain the ActionListeners for
 * every object in the FrontEnd
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package client.controller;

import client.views.*;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.net.Socket;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MainViewController extends Controller {

	private PrintWriter pw;
	private BufferedReader br;
	private MainViewDisplay theMainViewDisplay;
	private SearchToolDisplay theSearchToolDisplay;
	private InsertToolDisplay theInsertToolDisplay;
	private DecreaseToolDisplay theDecreaseToolDisplay;

	/**
	 * constructs an object of type MainViewController
	 *
	 * @param theMainViewDisplay     the MainViewDisplay
	 * @param theSearchToolDisplay   the SearchToolDisplay
	 * @param theInsertToolDisplay   the InsertToolDisplay
	 * @param theDecreaseToolDisplay the DecreaseToolDisplay
	 */
	public MainViewController(MainViewDisplay theMainViewDisplay, SearchToolDisplay theSearchToolDisplay,
			InsertToolDisplay theInsertToolDisplay, DecreaseToolDisplay theDecreaseToolDisplay, Socket aSocket) {
		super(aSocket);
		this.theMainViewDisplay = theMainViewDisplay;
		this.theSearchToolDisplay = theSearchToolDisplay;
		this.theInsertToolDisplay = theInsertToolDisplay;
		this.theDecreaseToolDisplay = theDecreaseToolDisplay;

		initComponents();
		initToolList();
	}

	/**
	 * Listens for the Sell tool button to be pressed and then open another window
	 * for selling items
	 *
	 */
	public class GoToDecreaseItem implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			theSearchToolDisplay.setVisible(false);
			theInsertToolDisplay.setVisible(false);
			theDecreaseToolDisplay.setVisible(true);
		}

	}

	/**
	 *
	 * Listens for the Add tool button to be pressed and then open another window
	 * for adding items
	 *
	 */
	public class GoToInsertTool implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			theInsertToolDisplay.setVisible(true);
			theDecreaseToolDisplay.setVisible(false);
			theSearchToolDisplay.setVisible(false);
		}

	}

	/**
	 *
	 * Listens for the Search tools button to be pressed and then open another
	 * window for searching items
	 *
	 */
	public class GoToSearchTool implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			theDecreaseToolDisplay.setVisible(false);
			theInsertToolDisplay.setVisible(false);
			theSearchToolDisplay.setVisible(true);

			if (!theMainViewDisplay.getList().isSelectionEmpty()) {

				theSearchToolDisplay
						.setToolInfo(theMainViewDisplay.getTheTools()[theMainViewDisplay.getList().getSelectedIndex()]);

			}
		}

	}

	/**
	 *
	 * Listens for the Refresh button to be pressed and then it refreshes the list
	 *
	 */
	public class RefreshList implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {

			initToolList();

		}
	}

	/**
	 *
	 * This is the listener for the window of the MainViewDipslay when the user
	 * closes the window, the socket is closed and the client is stopped
	 */
	public class WindowClosedList extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent e) {
			try {
				theSocket.close();
				System.exit(0);
			} catch (IOException ex) {
				System.out.println("Couldn't close socket");
			}

		}
	}

	/**
	 * Updates the Tool List with information from the server
	 */
	private void initToolList() {
		try {

			PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
			BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
			pw.println("LIST");

			String theItems = br.readLine();
			theMainViewDisplay.setTheTools(theItems.split(" @ "));
			theItems = theItems.replace(";", " ");
			String[] theItemsSplit = theItems.split(" @ ");
			pw.flush();
			theMainViewDisplay.setItems(theItemsSplit);

		} catch (IOException ex) {
			System.out.println("Connection error");
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	private class SaveResultToFileAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {

			String pathToFile = theMainViewDisplay.showSaveDialog();

			if (pathToFile != null) {

				try {
					PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
					BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));

					pw.println("GENERATE-ORDER");
					pw.println(pathToFile);

					String response = br.readLine();

					switch (response) {

					case "SUCCESS":
						JOptionPane.showMessageDialog(new JFrame(), "Order created!", ":)", JOptionPane.PLAIN_MESSAGE);
						break;
					case "FAILED":
						JOptionPane.showMessageDialog(new JFrame(), "Unable to generate order", ":(",
								JOptionPane.PLAIN_MESSAGE);
						break;
					}
				} catch (IOException ex) {
					System.out.println(ex.getMessage());
				} finally {
					if (pw != null) {
						pw.close();
					}
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Initializes the controller for the GUI. That is, it "wires" up all buttons to
	 * their respective actions.
	 */
	private void initComponents() {
		theMainViewDisplay.getRefresh().addActionListener(new RefreshList());
		theMainViewDisplay.getSearchButton().addActionListener(new GoToSearchTool());
		theMainViewDisplay.getAddTool().addActionListener(new GoToInsertTool());
		theMainViewDisplay.getSellButton().addActionListener(new GoToDecreaseItem());
		theMainViewDisplay.getGenerateOrder().addActionListener(new SaveResultToFileAction());
		theMainViewDisplay.addWindowListener(new WindowClosedList());
	}

}

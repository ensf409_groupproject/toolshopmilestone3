/**
 * abstract class which all the other controller classes are extended from, contains
 * theSocket for communication with the server
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 */
package client.controller;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public abstract class Controller {
	/**
	 * the Socket that communicates with the server.
	 */
	protected Socket theSocket;

	/**
	 * initializes the socket variable
	 * 
	 * @param aSocket
	 */
	protected Controller(Socket aSocket) {
		theSocket = aSocket;
	}
}

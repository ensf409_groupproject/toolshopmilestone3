/**
 *  This class controls the process of searching tools
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package client.controller;

import client.views.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SearchToolController extends Controller {

	private SearchToolDisplay theSearchToolDisplay;

	/**
	 * Constructs an object of type SearchToolController
	 *
	 * @param theSearchToolDisplay
	 */
	public SearchToolController(SearchToolDisplay theSearchToolDisplay, Socket aSocket) {
		super(aSocket);
		this.theSearchToolDisplay = theSearchToolDisplay;
		initController();
	}

	/**
	 *
	 * The following class called SearchByName searches items by entering their
	 * name.
	 *
	 */
	public class SearchByName implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
				BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
				pw.println("SEARCH-NAME");
				pw.println(theSearchToolDisplay.getInput().getText());

				
				String response = br.readLine();

				theSearchToolDisplay.setToolInfo(response);

			} catch (IOException ex) {

				System.out.println("Connection error");
			}
			theSearchToolDisplay.clearEntries();
		}

	}

	/**
	 *
	 * The following class called SearchById searches items by entering their ID.
	 *
	 */
	public class SearchById implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {

				PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
				pw.println("SEARCH-ID");
				pw.println(theSearchToolDisplay.getInput().getText());
				BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
				String response = br.readLine();

				theSearchToolDisplay.setToolInfo(response);

			} catch (IOException ex) {
				System.out.println("Connection error");
			}

		}

	}

	/**
	 * Initializes the controller for the GUI.
	 */
	private void initController() {
		theSearchToolDisplay.getSearchByNameButton().addActionListener(new SearchByName());
		theSearchToolDisplay.getSearchByIdButton().addActionListener(new SearchById());
	}
}
